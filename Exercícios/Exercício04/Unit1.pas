unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    StaticText1: TStaticText;
    ListBox1: TListBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    Button4: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
if Edit1.Text <> '' then
begin
ListBox1.Items.add(Edit1.Text);
end;
Edit1.Clear;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
ListBox1.Items.Delete(ListBox1.ItemIndex);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
ListBox1.Clear;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
ListBox1.Items[ListBox1.ItemIndex] := Edit1.Text;
If ListBox1.ItemIndex <> -1 Then
begin
Button4.Enabled:= true;
end;
end;

end.
